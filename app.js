const express = require("express");
const app = express();
const axios = require("axios");
const cors = require("cors");

app.use(
  cors({
    origin: "*",
  })
);



app.get("/export_gestionaleauto", (req, res) => {
  axios
    .get(
      "http://xml.gestionaleauto.com/francomancini/export_gestionaleauto.php"
    )
    .then(function (response) {
      // console.log(res);
      // res.json(response.data);
      return res.send(response.data);
    })
    .catch(function (error) {
      res.json(error);
    });
});

app.listen(3000, () => {
  console.log("Example app listening on port 3000!");
});
function parseXml(xml) {
  var dom = null;
  if (window.DOMParser) {
    try {
      dom = new DOMParser().parseFromString(xml, "text/xml");
    } catch (e) {
      dom = null;
    }
  } else if (window.ActiveXObject) {
    try {
      dom = new ActiveXObject("Microsoft.XMLDOM");
      dom.async = false;
      if (!dom.loadXML(xml))
        // parse error ..

        window.alert(dom.parseError.reason + dom.parseError.srcText);
    } catch (e) {
      dom = null;
    }
  } else alert("cannot parse xml string!");
  return dom;
}
